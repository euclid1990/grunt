#A simple guide to getting started with Grunt

##1. Getting started
To make sure that you have Node and npm installed:

    node -v
    npm -v

If you dont have above Node and npm run commands to install:

- Install Ruby with RVM and Compass gem:

        sudo curl -sSL https://get.rvm.io | bash -s stable --ruby
        rvm --default use 2.2.1                                 (2.2.1 is currently ruby version: # ruby -v)
        gem install compass

- Setup npm

        sudo apt-get install npm

- Update NodeJs

        sudo npm cache clean -f
        npm config set registry http://registry.npmjs.org/
        sudo npm install -g n
        sudo n stable

- Install Grunt CLI

        sudo npm install -g grunt-cli

##2. Overview
The file structure for our samples. Ex: `public` directory of MVC model in Web Framework:

    - public
    ----- js                            // Final Javascript files (minified files)
    ------------- app.js
    ------------- app.min.js
    ------------- coffeejs.js
    ------------- coffeejs.min.js
    ----- css                           // Final CSS files (minified files)
    ------------- library.css
    ------------- library.min.css
    ------------- style.css
    ------------- style.min.css
    ----- src                           // Original files
    --------- scss
    ------------- library.scss
    ------------- style.scss
    --------- js
    ------------- coffeejs.coffee
    ------------- library.js
    ------------- script.js
    - Gruntfile.js                      // Grunt configuration
    - package.json                      // npm pacakge configuration


##3. Prepare

###3.1. Installing The Packages

Running [# npm install] in the same folder as a package.json file will install the correct version of each dependency listed therein.

- Easy unit testing for node.js and the browser

        "grunt-contrib-nodeunit": "0.4.1"

- Static analysis tool for JavaScript

        "grunt-contrib-jshint": "0.11.2"

- Stylish reporter for JSHint

        "jshint-stylish": "2.0.0"

- Compile CoffeeScript files to JavaScript

        "grunt-contrib-coffee": "0.13.0"

- Concatenate files

        "grunt-contrib-concat": "0.5.1"

- Compile LESS files to CSS

        "grunt-contrib-uglify": "0.9.1"

- Compile SASS to CSS using Compass

        "grunt-contrib-compass": "1.0.3"

- Compile LESS files to CSS

        "grunt-contrib-less": "1.0.1"

- Minify CSS

        "grunt-contrib-cssmin": "0.12.3"

- Watch file patterns are added, changed or deleted

        "grunt-contrib-watch": "0.6.1"

###3.2. Grunt setup and configuration

Gruntfile.js file. This is the default place where our settings will go.

##4. Testing

Open terminal and execute command:

    grunt js

    grunt css

    grunt watch

![Grunt Watch](./Grunt_Watch.png)