/**
 * my-project-name
 * 2015-05-27
 */
function myLibraryFunction(p1, p2) {
    return p1 + p2;
}
var p1 = 'Java';
var p2 = 'Script';
console.log(myLibraryFunction(p1, p2));