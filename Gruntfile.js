/**
 * All Grunt configuration goes inside this function
 *
 */
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    // Get the configuration info from package.json
    pkg: grunt.file.readJSON('package.json'),

    project: {
      basePath: 'public',
      css     : '<%= project.basePath %>/css',
      js      : '<%= project.basePath %>/js',
      src     : '<%= project.basePath %>/src',
      sass    : '<%= project.src %>/sass',
      script  : '<%= project.src %>/script',
      banner  : '/**\n' +
                ' * <%= pkg.name %>\n' +
                ' * <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                ' */\n'
    },

    // Analysis JavaScript code
    jshint: {
      options: {
        // Use jshint-stylish to make our errors look and read good
        reporter: require('jshint-stylish')
      },
      // When this task is run, lint the Gruntfile and all js files in script folder
      build: ['<%= project.script %>/*.js', '<%= project.script %>/*.js']
    },

    // Compile coffeescript file.
    coffee: {
      build: {
        files: [{
            expand: true,
            cwd: '<%= project.script %>',
            src: ['*.coffee'],
            dest: '<%= project.js %>',
            ext: '.js'
        }]
      }
    },

    // Concat all javascript file.
    concat: {
      options: {
        banner: '<%= project.banner %>'
      },
      build: {
        src: ['<%= project.script %>/library.js', '<%= project.script %>/script.js'],
        dest: '<%= project.js %>/app.js',
      }
    },

    // Minify a javascript source file
    uglify: {
      options: {
        banner: '<%= project.banner %>'
      },
      build: {
        files: [{
            expand: true,
            cwd: '<%= project.js %>',
            src: ['*.js', '!*.min.js'],
            dest: '<%= project.js %>',
            ext: '.min.js'
        }]
      }
    },

    // Compass sass/scss style sheet to css
    compass: {
      dist: {
        options: {
          sassDir: '<%= project.sass %>',
          cssDir: '<%= project.css %>',
        }
      }
    },

    // Minify our CSS
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: '<%= project.css %>',
          src: ['*.css', '!*.min.css'],
          dest: '<%= project.css %>',
          ext: '.min.css'
        }]
      }
    },

    // Watch resource and auto update
    watch: {
      // For sass files
      sass: {
        files: ['<%= project.sass %>/*.scss'],
        tasks: ['css']
      },
      // for scripts files
      script: {
        files: ['<%= project.script %>/*.js', '<%= project.script %>/*.coffee'],
        tasks: ['jshint', 'js']
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s)
  grunt.registerTask('default', ['js', 'css']);
  // Javascript task(s)
  grunt.registerTask('js', ['coffee', 'concat', 'uglify']);
  // Build Css task(s)
  grunt.registerTask('css', ['compass', 'cssmin']);

};